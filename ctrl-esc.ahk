#Requires AutoHotkey v2.0

#include lib\TapHoldManager.ahk

thm := TapHoldManager(150, 150, 1, "$*")
thm.Add("CapsLock", CapsLockCallback)

CapsLockCallback(isHold, taps, state) {
    if isHold {
        if state = 1 {
            Send "{LCtrl down}"
        } else {
            Send "{LCtrl up}"
        }
    } else {
        Send "{Esc}"
    }
}

Esc::CapsLock